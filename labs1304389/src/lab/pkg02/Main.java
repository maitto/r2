/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab.pkg02;

/**
 *
 * @author Mortti
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EspressoMachine e1 = new EspressoMachine();
        
        e1.powerOnOff();
        e1.fillTank();
        e1.fillCoffee();
        e1.coffeeButton();
        e1.coffeeButton();
        e1.coffeeButton();
        e1.coffeeButton();
        e1.coffeeButton();
        e1.coffeeButton();
        e1.coffeeButton();
        e1.espressoButton();
        e1.espressoButton();
        e1.espressoButton();
        e1.espressoButton();
        e1.fillTank();
        e1.espressoButton();
        e1.fillCoffee();
        e1.espressoButton();
        e1.espressoButton();
        e1.espressoButton();
        e1.espressoButton();
        e1.espressoButton();
        e1.espressoButton();
        e1.rinse();
        e1.espressoButton();
     
    }
    
}
