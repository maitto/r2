/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab.pkg02;

/**
 *
 * @author Mortti
 */
public class EspressoMachine {
    private int waterLevel = 0;
    private int beanLevel = 0;
    private boolean powerOn;
    private boolean lidOn;
    private int cup;
    private int beanCoffee;
    private int beanEspresso;
    private int rinse;
    private int clean;
    private int cycle;
    private int maxCycle;
    
    public EspressoMachine (){
        this.waterLevel = 0;
        this.beanLevel = 0;
        this.powerOn = false;
        this.lidOn = true;
        this.cup = 100;
        this.beanCoffee = 10;
        this.beanEspresso = 8;
        this.rinse = 25;
        this.clean = 300;
        this.cycle = 0;
        this.maxCycle = 15;
        
        
    }
    
    public void espressoButton(){
        if (this.powerOn){
            if (this.cycle < this.maxCycle){
            if (this.waterLevel >= this.cup){
            if (this.beanEspresso <= this.beanLevel){
                this.waterLevel = this.waterLevel - cup;
                this.beanLevel = this.beanLevel - this.beanEspresso;
                this.cycle++;
               System.out.println("Enjoy your Espresso!");
       }
            else{
                System.out.println("Check bean level");
            }
            }
           else{
               System.out.println("Check water level");
           }
            }
        
            
            else{
                System.out.println("please rinse the machine");
            
            }
    }
    }
    public void coffeeButton(){
        if (this.powerOn){
            if (this.cycle < this.maxCycle){
            if (this.waterLevel >= this.cup){
            if (this.beanCoffee <= this.beanLevel){
                this.waterLevel = this.waterLevel - cup;
                this.beanLevel = this.beanLevel - this.beanCoffee;
                this.cycle++;
               System.out.println("Enjoy your Coffee!");
       }
            else{
                System.out.println("check bean level");
            }
            }
           else{
               System.out.println("Check water level");
           }
            }
            }
            else{
                System.out.println("please rinse the machine");
            }
        
    }
    public void rinse(){
        if (this.powerOn){
            if (this.waterLevel >= this.rinse){
                this.waterLevel = this.waterLevel - this.rinse;
                this.cycle = 0;
                System.out.println("Rinse complete");
            }
            else{
                System.out.println("Not enough water to rinse");
            }
        }
    }
    public void clean(){
        if (this.powerOn){
            if (this.waterLevel >= this.clean){
                this.waterLevel = this.waterLevel - this.clean;
                System.out.println("Cleaning complete");
            }
            else{
                System.out.println("Not enough water to clean");
            }
        }
    }
    public void powerOnOff(){
       if (this.powerOn){
           this.powerOn = false;
       }
       else{
           this.powerOn = true;
           System.out.println("Power is on!");
       }    
   }
      public void fillTank(){
       this.lidOn = false;
       this.waterLevel = 1000;
       System.out.println("Water tank is now filled");
       this.lidOn = true;
   }
      public void fillCoffee(){
          this.beanLevel = 100;
          System.out.println("Filled up with beans");
      }
    }
    
    

